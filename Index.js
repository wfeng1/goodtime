var fs = require("fs");
/* store the start date as index for search */
function index() {
    this.array = fs.readFileSync("index").toString().split(",");
    this.head = this.array[0];
    this.tail = this.array[this.array.length - 1];
}

index.prototype.getIndex = function(start, end) {
    var list = [];
    for (var i = 0, len = this.array.length; i < len; i++) {
        if (new Date(this.array[i]).getTime() > new Date(start).getTime() &&
            new Date(this.array[i]).getTime() < new Date(end).getTime()) {
            list.push(this.array[i]);
        }
    }
    return list;
}

index.prototype.getStart = function() {
    return this.head;
}

index.prototype.getEnd = function() {
    return this.tail;
}

module.exports = index;