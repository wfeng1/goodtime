var fs = require("fs");
var express = require("express");
var google = require("googleapis");
var jsonfile = require('jsonfile');
var moment = require("moment");

var LRU = require('./LRUCache');
var Index = require('./Index');

var LocalCalendar = require('./LocalCalendar');

var LRUCache = new LRU(100);
/**
 *load local cache file  to memory first.
 */
LRUCache.loadCache();

var app = express();

app.use(express.static('static'));


var server = app.listen(3001, function() {
    var host = "http://localhost";
    var port = "3001";
    console.log('Example app listening at http://%s:%s', host, port);

    /**
     *Write cache to the disk, so it persistants beyond server restarts.
     */
    setInterval(function() {
        var newCache = LRUCache.toJSON();
        jsonfile.writeFile("cache", newCache, function(err) {
            if (err) {
                console.log('Error writing cache file: ' + err);
                return;
            }
            console.log("save cache file !");
        });
    }, oneSecond = 1000 * 10);

});


var OAuth2 = google.auth.OAuth2;

var oauth2Client = new OAuth2("1045560707945-iluflo4gejsfpq3h7am5hl7hv1coqfp3.apps.googleusercontent.com", "ZNhGw-77K42tt030YssBta7b", "http://localhost:3001/oauth2callback");

var scopes = [
    'https://www.googleapis.com/auth/calendar'
];

var url = oauth2Client.generateAuthUrl({
    access_type: 'offline', 
    scope: scopes 
});

app.get("/url", function(req, res) {
    res.send(url);
    console.log(url);
});

app.get("/tokens", function(req, res) {

    var code = req.query.code;

    console.log(code);

    oauth2Client.getToken(code, function(err, tokens) {
        if (err) {
            console.log(err);
            res.send(err);
            return;
        }

        console.log(err);
        console.log(tokens);
        oauth2Client.setCredentials(tokens);
        storeToken(tokens);
        var calendar = new LocalCalendar(oauth2Client);
        res.send(tokens);

    });
});
/**
*store the credentials
*/
function storeToken(token) {
    var TOKEN_DIR = "./credentials/";
    try {
        fs.mkdirSync(TOKEN_DIR);
    } catch (err) {
        if (err.code != 'EEXIST') {
            throw err;
        }
    }
    var TOKEN_PATH = TOKEN_DIR + 'calendar-nodejs.json';
    fs.writeFile(TOKEN_PATH, JSON.stringify(token));
    console.log('Token stored to ' + TOKEN_PATH);
}

/**
 *Handle the get request from client.
 */
app.get("/calendar-event", function(req, res) {
    var startDate = req.query.startDate;
    var endDate = req.query.endDate;

    if (!isValidDate(startDate) || !isValidDate(endDate)) {
        res.send("date is not empty or valid");
        return;
    }

    var events = [];

    var index = new Index();
    var finalStart = index.getStart();
    var finalEnd = index.getEnd();
    var query = {};
    var start;
    var end;

    if (!startDate && !endDate) {
        
        start = finalStart;
        end = finalEnd;
        query["startDate"] = start;
        query["endDate"] = end;
        
    } else if (startDate && endDate) {

        start = new Date(startDate).toISOString();
        end = new Date(endDate).toISOString();
        query["startDate"] = start;
        query["endDate"] = end;

    } else if (startDate && !endDate) {

        start = new Date(startDate).toISOString();
        end = finalEnd;
        query["startDate"] = start;
        query["endDate"] = null;

    } else if (!startDate && endDate) {
        
        start = finalStart;
        end = new Date(endDate).toISOString();
        query["startDate"] = null;
        query["endDate"] = end;

    }

    var dateList = index.getIndex(start, end);
    for (var i = 0, len = dateList.length; i < len; i++) {
        var event = getEvents(dateList[i]);
        event = filter(event);
        events.push(event);
    }

    var result = {
        "status": 200,
        "query": query,
        "result": {
            "events": events
        }
    };

    res.send(result);

});

function getEvents(date) {
    var event = LRUCache.get(date);
    if (event === "NULL") {
        console.log("memory cache not hit");
        event = getEventsFromLocalCalender(date);
        LRUCache.set(date, event);
        return event;
    } else {
        console.log("memory cache hit");
        return event;
    }

}

function getEventsFromLocalCalender(date) {
    var events = jsonfile.readFileSync("calendar");
    return events[date];
}

function isValidDate(date) {
    var formats = [
        moment.ISO_8601,
    ];
    return moment(date, formats, true).isValid() || !date;
}

function filter(event) {
    var result = {};
    result["id"] = event["id"];
    result["status"] = event["status"];
    result["created"] = event["created"];
    result["updated"] = event["updated"];
    result["summary"] = event["summary"];
    result["description"] = event["description"];
    result["location"] = event["location"];
    result["creator"] = event["creator"];
    result["organizer"] = event["organizer"];
    var start = event.start.dateTime || event.start.date;
    start = new Date(start).toISOString();
    result["startDate"] = start;
    var end = event.start.dateTime;
    end = new Date(end).toISOString();
    result["endDate"] = end;
    result["attendees"] = event["attendees"];
    return result;
}