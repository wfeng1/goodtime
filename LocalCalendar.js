var fs = require("fs");
var jsonfile = require('jsonfile');
var google = require("googleapis");

/**
 *syschronize the data with Google Calendar.
 */
module.exports = function calendar(auth) {
    var calendar = google.calendar('v3');
    calendar.events.list({
        auth: auth,
        calendarId: 'primary',
        //timeMin: (new Date()).toISOString(),
        singleEvents: true,
        orderBy: 'startTime'
    }, function(err, response) {
        if (err) {
            console.log('The API returned an error: ' + err);
            return;
        }
        var events = response.items;
        if (events.length == 0) {
            console.log('No upcoming events found.');
        } else {
            var json = {};
            var startArray = [];
            for (var i = 0, len = events.length; i < len; i++) {
                var event = events[i];
                var start = event.start.dateTime || event.start.date;
                start = new Date(start).toISOString();
                startArray.push(start);
                json[start] = event;
            }

            fs.writeFile('index', startArray, function(err) {
                if (err) {
                    console.log('Error writing calendar file' + err);
                    return;
                }
            });

            jsonfile.writeFile("calendar", json, function(err) {
                if (err) {
                    console.log('Error writing calendar file: ' + err);
                    return;
                }
            })
        }
    });

    //calendar.prototype.calendarSync = function(argument) {
        // TODO
        //recieve the push notification from google watch API
        //update the cache
    //};
}